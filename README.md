## Asset

Interfaccia a:
# Assetic un Asset Management framework: https://travis-ci.org/kriswallsmith/assetic

Il modulo può essere usato con le applicazioni di Zend Framework e Zend Expressive.
Fornisce le funzionalità per caricare asset e file che non sono necessariamente
nelle directory public del sito, utilizzando file di configurazione.

Gli asset manager sono caricati tramite Middleware e file di configurazione.

esempio - inserimento nel file /config/pipeline.php di un'applicazione di Zend Expressive:
``` php

//...............

// Register the dispatch middleware in the middleware pipeline
$app->pipeDispatchMiddleware();

$app->pipe(\Vigazzola\Asset\Middleware\Aggregate::class);

// At this point, if no Response is returned by any middleware, the
// NotFoundHandler kicks in; alternately, you can provide other fallback
// middleware to execute.
$app->pipe(NotFoundHandler::class);

```
I Middleware utilizzabili rispecchiano i diversi modi in cui possono essere organizzati
gli asset e per ogni modalità c'è una configrazione possibile.

## Middleware

# Map
Ogni risorsa richiesta ha un suo file con un rapporto 1 a 1.

in: /config/pipeline.php
``` php
$app->pipe(\Vigazzola\Asset\Middleware\Map::class);
```
Esempio di file di configurazione:

``` php

<?php
$baseDir = dirname(__FILE__) ;

return [
    'asset' => [
        'resolver' => [
            'map'   => [
                  '/test.js'   => $baseDir . '/../../test/public/test.js'
            ],            
        ]
    ]
]

```
# Path
La risorsa richiesta viene cercata all'interno dei paths configurati.
Le Directory in cui è ricercata la risorsa sono inserite con specifiche priorità,
Le directory possono essere aggiunte tramite array:

['path' =>realpath($baseDir . '/../../test/backbone'), 'priority' => 1000],

Oppure tramite stringa:

realpath($baseDir . '/../../test/backbone')

Nell'ultimo caso viene inserita una priorità di default (1).

in: /config/pipeline.php
``` php
$app->pipe(\Vigazzola\Asset\Middleware\Path::class);
```

Esempio di file di configurazione:

``` php

<?php
$baseDir = dirname(__FILE__) ;

return [
    'asset' => [
        'resolver' => [
            'paths' => [
                ['path' =>realpath($baseDir . '/../../test/backbone'), 'priority' => 1000],
                ['path' =>realpath($baseDir . '/../../test/public'), 'priority' => 100]
            ]
        ]
    ]
]

```

# Collection
L'asset richiesto è formato dall'insieme di risorse definite in configurazione.

in: /config/pipeline.php
``` php
$app->pipe(\Vigazzola\Asset\Middleware\Collection::class);
```

Esempio di file di configurazione:

``` php

<?php
$baseDir = dirname(__FILE__) ;

return [
    'asset' => [
        'resolver' => [
            'collections'   => [
                '/js/backbone-base.js' => [
                    '/file1.js',
                    '/file2.js',
                    '/file3.js',
                    //.....
                    '/filen.js',
                ],
        ]
    ]
]

```
Ogni asset che forma la collezione è cercato tramite un aggregate resolver,
che è l'insieme dei resolver presenti.

Di base i resolver sono:

``` php

Vigazzola\Asset\Resolvers::class        => function($container) {
    return [
        Vigazzola\Asset\Resolver\MapResolver::class         => 4000,
        Vigazzola\Asset\Resolver\CollectionResolver::class  => 2000,
        Vigazzola\Asset\Resolver\DirectoriesResolver::class => 1000,
        Vigazzola\Asset\Resolver\PathStackResolver::class   => 100
    ] ;
},

```
Eventuali filtri definiti sono applicati ad ogni asset della collezione.

# Directories
Una risoorsa è formata da tutti i file presenti nella directory definita in
configurazione. I file sono aggiunti ricorsivamente.

in: /config/pipeline.php
``` php
$app->pipe(\Vigazzola\Asset\Middleware\Directories::class);
```

La configurazione può avere 2 forme.
Semplice - il MimeType è definito da i file prenseti nelle direcotry.
l'elenco delle directory coinvolte, con o senza priorità (vedi CollectionResolver):

``` php

<?php
$baseDir = dirname(__FILE__) ;

return [
    'asset' => [
        'resolver' => [
            'directories' => [
                '/js/backbone-base.js' => [
                        $baseDir . '/../../test/backbone',
                ]
            ],
        ]
    ]
]
```

Oppure forzando il MimeType in configurazione:

    ``` php

    <?php
    $baseDir = dirname(__FILE__) ;

    return [
        'asset' => [
            'resolver' => [
                'directories' => [
                    '/js/backbone-base.js' => [
                        'mimetype'      => 'text/ajavascript',
                        'directories'   => [
                            __DIR__ . '/../assets/template/application',
                        ]
                    ],
                ]
            ]
        ]
    ```

Eventuali filtri definiti sono applicati ad ogni asset della collezione.

# Aggregate
Usa tutti i resolver definiti.

Di base i resolver sono:

``` php

Vigazzola\Asset\Resolvers::class        => function($container) {
    return [
        Vigazzola\Asset\Resolver\MapResolver::class         => 4000,
        Vigazzola\Asset\Resolver\CollectionResolver::class  => 2000,
        Vigazzola\Asset\Resolver\DirectoriesResolver::class => 1000,
        Vigazzola\Asset\Resolver\PathStackResolver::class   => 100
    ] ;
},

```
# Cache304
Questo Middleware se inserite prima degli altri, verica la presenza della risorsa in
cache, ne verifica la data e se è se non è scaduta torna un 304.

in: pipeline.php

// Register the dispatch middleware in the middleware pipeline
$app->pipeDispatchMiddleware();

$app->pipe(Vigazzola\Asset\Middleware\Cache304::class);
// ......
$app->pipe(\Vigazzola\Asset\Middleware\Aggregate::class);

## Cache
Attualmente l'unico Cache Provider e su MongoDB.
Per escludere l'uso della cache:

``` php
'dependencies' => [
    'factories' => [
        'Asset.Cache.Provider'         => function($sm, $name = null, $options = null) {
            return null ;
        }
    ]
]
```
Settaggi possibili.
Parametri per la connessione al DB:

``` php
'asset' => [
    'cache' => [
        'MongoDB'=> [
            'params'    => [
                'uri'            => 'mongodb://localhost:27017',
                'uriOptions'     => [],
                'driverOptions'  => [],
                'db'             => 'xxxxxxxxx',
            ],
        ],
    ]
]
```

Il nome della collection di MongoDB dove viene registrata la cache:

``` php
'asset' => [
    'cache' => [
        'MongoDB'=> [
            'collection'  => 'cacheAsset'
        ],
    ]
]
```

Il Lifetime della cache, espresso in secondi:
``` php
    'asset' => [
        'cache' => [
            'LifeTime'  => 60
        ]
    ]
```

## Filtri
Agli Asset possono essere applicati dei filtri, per poterli usare vanno definiti e
associati all'asset tramite nome o al mime-type.

I Filtri possono essere cosi' associati all'asset:

``` php
'asset' => [
    'filters' => [
        'filters' => [
            '<nome_asset>' => [
                // Come array definendo la priorità
                ['filter' => '<nome_filtro>',   'priority' => <priorita - numerico>],

                // Come array definendo la priorità ed eventuali opzioni come array
                ['filter' => '<nome_filtro>',   'priority' => <priorita - numerico>, 'options' => <array_optioni>],

                // come stringa, priorità di default == 1
                'AddFileNameFilter'
            ],
        ],

        // Filters by mime-type
        'text/javascript' => [
                // ADD FIILTERS
        ],

        // Filters by Extension
        'php' => [
            // ADD FIILTERS
        ]
    ]    
]
```  

Attualmente sono definiti 3 Filtri a titolo d'esempio.

Uno definito nella classe: \Vigazzola\Asset\Filter\AddFileNameFilter
Aggiunge, come commento, il path del file in testo all'output della risorsa.

Come assegnarlo, ad es.:

``` php
'asset' => [
    'filters' => [
        'filters' => [
            '/js/test.js' => [
                ['filter' => 'AddFileNameFilter',   'priority' => 100],
                // Senza priorita
                'AddFileNameFilter'
            ],
        ],

        // Filters by mime-type
        'text/javascript' => [
                // ADD FIILTERS
        ],

        // Filters by Extension
        'php' => [
            // ADD FIILTERS
        ]
    ]    
]
```  
Uno definito nella classe: \Vigazzola\Asset\Filter\TmplDataFilter
Crea una funzione Javascript che richiama il templater tmpl().
https://github.com/blueimp/JavaScript-Templates

Come assegnarlo, ad es.:

``` php
'asset' => [
    'filters' => [
        'filters' => [
            '/js/test.js' => [
                ['filter' => 'TmplDataFilter',   'priority' => 100, 'options' => ['path' => '<base_path>']],

                // Senza priorita
                'TmplDataFilter'
            ],
        ],

        // Filters by mime-type
        'text/javascript' => [
                // ADD FIILTERS
        ],

        // Filters by Extension
        'php' => [
            // ADD FIILTERS
        ]
    ]    
]
```  

L'altro preso da assetic: \Assetic\Filter\JSMinFilter::class
Per aggiungere nuovi filtri, settare in configurazione:

``` php
'asset' => [
    'resolver' => [
        'map'   => [

        ],
        'paths' => [
        ]
    ],
    'filters' => [
        'dependencies' => [
            'aliases'    => [
                    'JSMinFilter'       => \Assetic\Filter\JSMinFilter::class,
                    'jsMinFilter'       => \Assetic\Filter\JSMinFilter::class,
                    'jsminfilter'       => \Assetic\Filter\JSMinFilter::class,
            ],
            'factories' => [
                \Assetic\Filter\JSMinFilter::class => InvokableFactory::class
            ]
        ],
    ]
],
```  
