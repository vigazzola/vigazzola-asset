<?php
/**
 * @author vigazzola@gmail.com
 */
namespace Vigazzola\Asset\Resolver ;

use Psr\Container\ContainerInterface;

class MapResolverFactory
{
    /**
     */
    public function __invoke(ContainerInterface $container) 
    {
        $config = $container->get('config');
        $map    = [];

        if (isset($config['asset']['resolver']['map'])) {
            $map = $config['asset']['resolver']['map'];
        }
        
        $Resolve = new MapResolver();
        
        $Resolve->setMimeResolver($container->get(\Vigazzola\Asset\Resolver\MimeResolver::class)) ;
        $Resolve->setConfig($map) ;        
        
        return $Resolve ;
    }
}
