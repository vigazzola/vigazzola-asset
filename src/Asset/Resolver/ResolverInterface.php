<?php
/**
 *
 */
namespace Vigazzola\Asset\Resolver ;

use Vigazzola\Asset\Filter\AssetFilterManagerInterface ;

interface ResolverInterface
{
     /**
     * Set the config
     *
     * @param array $config
     */
   public function setConfig(array $config) ;

    /**
     * Resolve an Asset
     *
     * @param   string  $path   The path to resolve.
     *
     * @return  \Assetic\Asset\AssetInterface|null Asset instance when found, null when not.
     */
    public function resolve($path, AssetFilterManagerInterface $filterManager = null);

    /**
     * Set the MimeResolver.
     *
     * @param MimeResolver $resolver
     */
    public function setMimeResolver(MimeResolver $resolver);

    /**
     * Get the MimeResolver
     *
     * @return MimeResolver
     */
    public function getMimeResolver();
}
