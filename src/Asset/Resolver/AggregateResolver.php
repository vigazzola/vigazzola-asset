<?php
/**
 *
 */
namespace Vigazzola\Asset\Resolver ;

use Psr\Container\ContainerInterface;

use Assetic\Asset\FileAsset;
use Assetic\Asset\HttpAsset;

use SplFileInfo;
use Zend\Stdlib\SplStack;
use Zend\Stdlib\PriorityQueue;
use Vigazzola\Asset\Filter\AssetFilterManagerInterface ;

class AggregateResolver implements  ResolverInterface
{
    private $resolvers ;
    private $mimeResolver ;
    private $container ;

    /**
     * Constructor
     */
    public function __construct(ContainerInterface $container)
    {
        $this->resolvers = new PriorityQueue();
        $this->container = $container ;
    }

    /**
     * Set the config
     *
     * @param array $config
     */
    public function setConfig(array $resolvers) {
        if (!is_array($resolvers) && !$paths instanceof Traversable) {
            throw new \Exception(sprintf(
            'Invalid argument provided for $paths, expecting either an array or Traversable object, "%s" given',
            is_object($resolvers) ? get_class($resolvers) : gettype($resolvers)
            ));
        }

        $this->clearResolvers();
        foreach ($resolvers as $resolver => $priority) {
            $this->addResolver($resolver, $priority);
        }

    }

    public function clearResolvers()
    {
        $this->resolvers = new PriorityQueue();
    }

    public function getResolvers()
    {
        return $this->resolvers ;
    }

    public function addResolver($resolver, $priority = 1)
    {
        if (is_string($resolver)) {
            $resolver = $this->container->get($resolver);
        }

        if (!$resolver instanceof ResolverInterface) {
            throw new \Exception(
                'Service does not implement the required interface ResolverInterface.'
            );
        }

        if ($resolver instanceof AggregateResolverInterface) {
            $resolver->setAggregateResolver($this) ;
        }

        $this->resolvers->insert($resolver, $priority);

        return $this ;
    }

    /**
     * Resolve an Asset
     *
     * @param   string  $name   The path to resolve.
     *
     * @return  \Assetic\Asset\AssetInterface|null Asset instance when found, null when not.
     */
    public function resolve($name, AssetFilterManagerInterface $filterManager = null){
        foreach ($this->getResolvers() as $Resolver) {
            $resource = $Resolver->resolve($name, $filterManager);
            if (null !== $resource) {
                return $resource;
            }
        }

        return null;
    }

    /**
     * Set the MimeResolver.
     *
     * @param MimeResolver $resolver
     */
    public function setMimeResolver(MimeResolver $resolver) {
        $this->mimeResolver = $resolver ;

        return $this ;
    }

    /**
     * Get the MimeResolver
     *
     * @return MimeResolver
     */
    public function getMimeResolver() {
        return $this->mimeResolver ;
    }

    /**
     * {@inheritDoc}
     */
    public function collect()
    {
        $collection = [];
        foreach ($this->getResolvers as $resolver) {
            if (!method_exists($resolver, 'collect')) {
                continue;
            }

            $collection = array_merge($resolver->collect(), $collection);
        }

        return array_unique($collection);
    }

}
