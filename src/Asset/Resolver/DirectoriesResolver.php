<?php
/**
 *
 */
namespace Vigazzola\Asset\Resolver ;

use Assetic\Asset\AssetCollection;
use Assetic\Asset\AssetInterface;
use Assetic\Asset\FileAsset;

use DirectoryIterator;
use SplFileInfo;
use Zend\Stdlib\SplStack;
use Zend\Stdlib\PriorityQueue;
use Vigazzola\Asset\Filter\AssetFilterManagerInterface ;

class DirectoriesResolver implements  ResolverInterface, AggregateResolverInterface
{
    private $dirs ;
    private $mimeResolver ;
    private $aggregateResolver ;

    /**
     * Flag indicating whether or not LFI protection for rendering view scripts is enabled
     *
     * @var bool
     */
    protected $lfiProtectionOn = true;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dirs = [] ;
    }

    /**
     * Set the config
     *
     * @param array $config
     */
    public function setConfig(array $paths) {
        if (!is_array($paths) && !$paths instanceof Traversable) {
            throw new \Exception(sprintf(
            'Invalid argument provided for $directories, expecting either an array or Traversable object, "%s" given',
            is_object($paths) ? get_class($paths) : gettype($paths)
            ));
        }

        $this->clearPaths();
        foreach ($paths as $name => $paths) {
            $this->addPath($name, $paths);
        }
    }

    public function clearPaths()
    {
        $this->dirs = [];
    }

    public function getPaths()
    {
        return $this->dirs ;
    }

    public function addPath(string $name, array $paths)
    {
        $this->dirs[$name] = [
            'mimetype'      => null,
            'directories'   => new PriorityQueue()
        ] ;
        if(isset($paths['mimetype'])) {
            $this->dirs[$name]['mimetype'] = $paths['mimetype'] ;
        }
        if(isset($paths['directories'])) {
            $paths = $paths['directories'] ;
        }

        foreach($paths as $path) {
            if (is_string($path)) {
                $this->dirs[$name]['directories']->insert($this->normalizePath($path), 1);
                continue ;
            }
            elseif (!is_array($path) && !$path instanceof ArrayAccess) {
                throw new \Exception(sprintf(
                    'Provided path must be an array or an instance of ArrayAccess, %s given',
                    is_object($path) ? get_class($path) : gettype($path)
                ));
            }
            elseif (isset($path['priority']) && isset($path['path'])) {
                $this->dirs[$name]['directories']->insert($this->normalizePath($path['path']), $path['priority']);

                continue ;
            }
            throw new \Exception( 'Provided array must contain both keys "priority" and "path"');
        }
    }

    /**
     * Set LFI protection flag
     *
     * @param  bool $flag
     * @return self
     */
    public function setLfiProtection($flag)
    {
        $this->lfiProtectionOn = (bool) $flag;
    }

    /**
     * Return status of LFI protection flag
     *
     * @return bool
     */
    public function isLfiProtectionOn()
    {
        return $this->lfiProtectionOn;
    }

    /**
     * Resolve an Asset
     *
     * @param   string  $name   The path to resolve.
     *
     * @return  \Assetic\Asset\AssetInterface|null Asset instance when found, null when not.
     */
    public function resolve($name, AssetFilterManagerInterface $filterManager = null){
        if ($this->isLfiProtectionOn() && preg_match('#\.\.[\\\/]#', $name)) {
            return null;
        }

        $dirs = $this->getPaths() ;
        if (!isset($dirs[$name])) {
            return null;
        }

        $collection = new AssetCollection;
        $mimeType   = null;
        $collection->setTargetPath($name);

        foreach ($dirs[$name]['directories'] as $asset) {
            $files  = $this->getRecursiveFiles($asset) ;

            foreach ($files as $file) {
                $partialPath = str_replace($asset, "", strstr($file, $asset)) ;
                if($partialPath[0]) {
                    $partialPath = substr($partialPath, 1) ;
                }

                $SplFileInfo = new SplFileInfo($file);

                $res = null ;
                if ($SplFileInfo->isReadable() && !$SplFileInfo->isDir()) {
                    $filePath = $SplFileInfo->getRealPath();
                    $mimeType = $this->getMimeResolver()->getMimeType($filePath);
                    $res    = new FileAsset($filePath);

                    $res->mimetype = $mimeType;
                }

                if ($res === null) {
                    throw new Exception\RuntimeException("Asset '$file' could not be found.");
                }

                if (!$res instanceof AssetInterface) {
                    throw new Exception\RuntimeException(
                        "Asset '$file' does not implement Assetic\\Asset\\AssetInterface."
                        );
                }

                if (null !== $mimeType && $res->mimetype !== $mimeType) {
                    throw new Exception\RuntimeException(sprintf(
                    'Asset "%s" from collection "%s" doesn\'t have the expected mime-type "%s".',
                    $asset,
                    $name,
                    $mimeType
                    ));
                }

                $mimeType = $res->mimetype;

                if($filterManager) {
                    $filterManager->setFilters($name, $res);
                }

                $collection->add($res);
            }
        }
        
        $collection->mimetype = $mimeType ;
        if($dirs[$name]['mimetype']) {
            $collection->mimetype = $dirs[$name]['mimetype'] ;
        }

        return $collection;
    }

    public function getAggregateResolver() {
        return $this->aggregateResolver ;
    }

    public function setAggregateResolver($resolver) {
        if(!$resolver instanceof  ResolverInterface) {
            throw new Exception\RuntimeException(
                "Resolver '$resolver' does not implement Vigazzola\\Asset\\Resolver\\ResolverInterface."
            );
        }
        $this->aggregateResolver = $resolver ;

        return $this ;
    }


    /**
     * Set the MimeResolver.
     *
     * @param MimeResolver $resolver
     */
    public function setMimeResolver(MimeResolver $resolver) {
        $this->mimeResolver = $resolver ;

        return $this ;
    }

    /**
     * Get the MimeResolver
     *
     * @return MimeResolver
     */
    public function getMimeResolver() {
        return $this->mimeResolver ;
    }

    /**
     * {@inheritDoc}
     */
    public function collect()
    {
        return array_keys($this->dirs);
    }

    ////////////////////////////////////////////////////////////////////////
    protected function normalizePath($path)
    {
        $path = rtrim($path, '/\\');
        $path .= DIRECTORY_SEPARATOR;

        return $path;
    }

    /**
     * Get dir's files
     */
    protected function getFiles($dir, &$files = array()) {
        $files = [] ;
        foreach ($dir as $path) {
            $file = new SplFileInfo($path . $dir);
            if (!$file->isReadable() || !$file->isDir()) continue ;

            $files = $this->getRecursiveFiles($path . $dir) ;
        }

        return $files ;
    }

    private function getRecursiveFiles($path) {
        $files = array() ;
        $DirectoryIterator = new DirectoryIterator($path) ;

        $dirs = array() ;
        foreach ($DirectoryIterator as $File) {
            if (!$File->isReadable() || $File->isDot()) continue ;

            if($File->isDir()) {
                $dirs[] = $File->getPathname() ;
                continue ;
            }

            $files[] = $File->getPathname() ;
            sort($files) ;
        }

        sort($dirs) ;
        foreach ($dirs as $dir){
            $files = array_merge($files, $this->getRecursiveFiles($dir)) ;
        }

        return $files ;
    }

}
