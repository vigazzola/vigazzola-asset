<?php
/**
 *
 */
namespace Vigazzola\Asset\Resolver ;

use Assetic\Asset\FileAsset;
use Assetic\Asset\HttpAsset;
use Vigazzola\Asset\Filter\AssetFilterManagerInterface ;

class MapResolver implements  ResolverInterface
{
    private $map ;
    private $mimeResolver ;

     /**
     * Set the config
     *
     * @param array $config
     */
   public function setConfig(array $map) {
       $this->map = $map ;
   }

    /**
     * Resolve an Asset
     *
     * @param   string  $path   The path to resolve.
     *
     * @return  \Assetic\Asset\AssetInterface|null Asset instance when found, null when not.
     */
    public function resolve($path, AssetFilterManagerInterface $filterManager = null){
        if (!isset($this->map[$path])) {
            return null;
        }

        $file            = $this->map[$path];
        $mimeType        = $this->getMimeResolver()->getMimeType($file);

        if (false === filter_var($file, FILTER_VALIDATE_URL)) {
            $asset = new FileAsset($file);
        } else {
            $asset = new HttpAsset($file);
        }

        $asset->mimetype = $mimeType;

        if($filterManager) {
            $filterManager->setFilters($path, $asset);
        }

        return $asset;
    }

    /**
     * Set the MimeResolver.
     *
     * @param MimeResolver $resolver
     */
    public function setMimeResolver(MimeResolver $resolver) {
        $this->mimeResolver = $resolver ;

        return $this ;
    }

    /**
     * Get the MimeResolver
     *
     * @return MimeResolver
     */
    public function getMimeResolver() {
        return $this->mimeResolver ;
    }
}
