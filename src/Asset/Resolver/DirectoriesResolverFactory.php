<?php
/**
 * @author vigazzola@gmail.com
 */
namespace Vigazzola\Asset\Resolver ;

use Psr\Container\ContainerInterface;

class DirectoriesResolverFactory
{
    /**
     */
    public function __invoke(ContainerInterface $container)
    {
        $config         = $container->get('config');
        $directories    = [];

        if (isset($config['asset']['resolver']['directories'])) {
            $directories = $config['asset']['resolver']['directories'];
        }

        $Resolve = new DirectoriesResolver();

        $Resolve->setMimeResolver($container->get(\Vigazzola\Asset\Resolver\MimeResolver::class)) ;
        $Resolve->setConfig($directories) ;

        return $Resolve ;
    }
}
