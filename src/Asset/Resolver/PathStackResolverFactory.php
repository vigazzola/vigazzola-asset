<?php
/**
 * @author vigazzola@gmail.com
 */
namespace Vigazzola\Asset\Resolver ;

use Psr\Container\ContainerInterface;

class PathStackResolverFactory
{
    /**
     */
    public function __invoke(ContainerInterface $container) 
    {
        $config = $container->get('config');
        $paths  = [];

        if (isset($config['asset']['resolver']['paths'])) {
            $paths = $config['asset']['resolver']['paths'];
        }

        $Resolve = new PathStackResolver() ;
        $Resolve->setMimeResolver($container->get(\Vigazzola\Asset\Resolver\MimeResolver::class)) ;        
        $Resolve->setConfig($paths) ;
        
        return $Resolve ;
    }
}
