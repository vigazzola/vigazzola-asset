<?php
/**
 * @author vigazzola@gmail.com
 */
namespace Vigazzola\Asset\Resolver ;

use Psr\Container\ContainerInterface;

class AggregateResolverFactory
{
    /**
     */
    public function __invoke(ContainerInterface $container)
    {
        $config     = $container->get('config');
        $resolvers  = $container->get(\Vigazzola\Asset\Resolvers::class) ;

        $Resolver = new AggregateResolver($container);
        $Resolver->clearResolvers();

        foreach ($resolvers as $resolver => $priority) {
            $Resolver->addResolver($resolver, $priority);
        }
        $Resolver->setMimeResolver($container->get(\Vigazzola\Asset\Resolver\MimeResolver::class) ) ;

        return $Resolver ;
    }
}
