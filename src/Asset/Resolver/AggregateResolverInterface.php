<?php
/**
 *
 */
namespace Vigazzola\Asset\Resolver ;

interface AggregateResolverInterface
{
    public function getAggregateResolver() ;

    public function setAggregateResolver($resolver) ;
}
