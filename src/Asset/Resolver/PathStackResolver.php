<?php
/**
 *
 */
namespace Vigazzola\Asset\Resolver ;

use Assetic\Asset\FileAsset;
use Assetic\Asset\HttpAsset;

use SplFileInfo;
use Zend\Stdlib\SplStack;
use Zend\Stdlib\PriorityQueue;
use Vigazzola\Asset\Filter\AssetFilterManagerInterface ;

class PathStackResolver implements  ResolverInterface
{
    private $paths ;
    private $mimeResolver ;

    /**
     * Flag indicating whether or not LFI protection for rendering view scripts is enabled
     *
     * @var bool
     */
    protected $lfiProtectionOn = true;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->paths = new PriorityQueue();
    }

    /**
     * Set the config
     *
     * @param array $config
     */
    public function setConfig(array $paths) {
        if (!is_array($paths) && !$paths instanceof Traversable) {
            throw new \Exception(sprintf(
            'Invalid argument provided for $paths, expecting either an array or Traversable object, "%s" given',
            is_object($paths) ? get_class($paths) : gettype($paths)
            ));
        }

        $this->clearPaths();
        foreach ($paths as $path) {
            $this->addPath($path);
        }
    }

    public function clearPaths()
    {
        $this->paths = new PriorityQueue();
    }

    public function getPaths()
    {
        return $this->paths ;
    }

    public function addPath($path, $priority = 1)
    {
        if (is_string($path)) {
            $this->paths->insert($this->normalizePath($path), $priority);

            return;
        }

        if (!is_array($path) && !$path instanceof ArrayAccess) {
            throw new \Exception(sprintf(
                'Provided path must be an array or an instance of ArrayAccess, %s given',
                is_object($path) ? get_class($path) : gettype($path)
            ));
        }

        if (isset($path['priority']) && isset($path['path'])) {
            $this->paths->insert($this->normalizePath($path['path']), $path['priority']);

            return;
        }

        throw new \Exception( 'Provided array must contain both keys "priority" and "path"');
    }

    /**
     * Set LFI protection flag
     *
     * @param  bool $flag
     * @return self
     */
    public function setLfiProtection($flag)
    {
        $this->lfiProtectionOn = (bool) $flag;
    }

    /**
     * Return status of LFI protection flag
     *
     * @return bool
     */
    public function isLfiProtectionOn()
    {
        return $this->lfiProtectionOn;
    }

    /**
     * Resolve an Asset
     *
     * @param   string  $name   The path to resolve.
     *
     * @return  \Assetic\Asset\AssetInterface|null Asset instance when found, null when not.
     */
    public function resolve($name, AssetFilterManagerInterface $filterManager = null){
        if ($this->isLfiProtectionOn() && preg_match('#\.\.[\\\/]#', $name)) {
            return null;
        }

        foreach ($this->getPaths() as $path) {
            $file = new SplFileInfo($path . $name);

            if ($file->isReadable() && !$file->isDir()) {
                $filePath = $file->getRealPath();
                $mimeType = $this->getMimeResolver()->getMimeType($filePath);
                $asset    = new FileAsset($filePath);

                $asset->mimetype = $mimeType;

                if($filterManager) {
                    $filterManager->setFilters($name, $asset);
                }

                return $asset;
            }
        }

        return null;
    }

    /**
     * Set the MimeResolver.
     *
     * @param MimeResolver $resolver
     */
    public function setMimeResolver(MimeResolver $resolver) {
        $this->mimeResolver = $resolver ;

        return $this ;
    }

    /**
     * Get the MimeResolver
     *
     * @return MimeResolver
     */
    public function getMimeResolver() {
        return $this->mimeResolver ;
    }

    /**
     * {@inheritDoc}
     */
    public function collect()
    {
        $collection = array();
        foreach ($this->getPaths() as $path) {
            $locations = new SplStack();
            $pathInfo = new SplFileInfo($path);
            $locations->push($pathInfo);
            $basePath = $this->normalizePath($pathInfo->getRealPath());

            while (!$locations->isEmpty()) {
                /** @var SplFileInfo $pathInfo */
                $pathInfo = $locations->pop();
                if (!$pathInfo->isReadable()) {
                    continue;
                }
                if ($pathInfo->isDir()) {
                    $dir = new DirectoryResource($pathInfo->getRealPath());
                    foreach ($dir as $resource) {
                        $locations->push(new SplFileInfo($resource));
                    }
                } elseif (!isset($collection[$pathInfo->getPath()])) {
                    $collection[] = substr($pathInfo->getRealPath(), strlen($basePath));
                }
            }
        }

        return $collection;
    }

    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    protected function normalizePath($path)
    {
        $path = rtrim($path, '/\\');
        $path .= DIRECTORY_SEPARATOR;

        return $path;
    }

}
