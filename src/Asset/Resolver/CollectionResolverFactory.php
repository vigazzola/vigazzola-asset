<?php
/**
 * @author vigazzola@gmail.com
 */
namespace Vigazzola\Asset\Resolver ;

use Psr\Container\ContainerInterface;

class CollectionResolverFactory
{
    /**
     */
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $collections    = [];

        if (isset($config['asset']['resolver']['collections'])) {
            $collections = $config['asset']['resolver']['collections'];
        }

        $Resolve = new CollectionResolver();

        $Resolve->setMimeResolver($container->get(\Vigazzola\Asset\Resolver\MimeResolver::class)) ;
        $Resolve->setConfig($collections) ;

        return $Resolve ;
    }
}
