<?php
/**
 *
 */
namespace Vigazzola\Asset\Resolver ;

use Assetic\Asset\AssetCollection;
use Assetic\Asset\AssetInterface;
use Vigazzola\Asset\Filter\AssetFilterManagerInterface ;

class CollectionResolver implements  ResolverInterface, AggregateResolverInterface
{
    private $collections ;
    private $mimeResolver ;
    private $aggregateResolver ;

     /**
     * Set the config
     *
     * @param array $config
     */
   public function setConfig(array $collections) {
       $this->collections = $collections ;
   }

    /**
     * Resolve an Asset
     *
     * @param   string  $name   The path to resolve.
     *
     * @return  \Assetic\Asset\AssetInterface|null Asset instance when found, null when not.
     */
    public function resolve($name, AssetFilterManagerInterface $filterManager = null){
        if (!isset($this->collections[$name])) {
            return null;
        }

        if (!is_array($this->collections[$name])) {
            throw new \Exception(
                "Collection with name $name is not an an array."
            );
        }

        $collection = new AssetCollection;
        $mimeType   = null;
        $collection->setTargetPath($name);
        foreach ($this->collections[$name] as $asset) {

            if (!is_string($asset)) {
                throw new \Exception(
                    'Asset should be of type string. got ' . gettype($asset)
                );
            }

            if (null === ($res = $this->getAggregateResolver()->resolve($asset))) {
                throw new \Exception("Asset '$asset' could not be found.");
            }

            if (!$res instanceof AssetInterface) {
                throw new \Exception(
                    "Asset '$asset' does not implement Assetic\\Asset\\AssetInterface."
                );
            }

            if (null !== $mimeType && $res->mimetype !== $mimeType) {
                throw new \Exception(sprintf(
                    'Asset "%s" from collection "%s" doesn\'t have the expected mime-type "%s".',
                    $asset,
                    $name,
                    $mimeType
                ));
            }

            $mimeType = $res->mimetype;

            if($filterManager) {
                $filterManager->setFilters($name, $res);
            }

            $collection->add($res);
        }

        $collection->mimetype = $mimeType;

        return $collection;
    }

    public function getAggregateResolver() {
        return $this->aggregateResolver ;
    }

    public function setAggregateResolver($resolver) {
        if(!$resolver instanceof  ResolverInterface) {
            throw new Exception\RuntimeException(
                "Resolver '$resolver' does not implement Vigazzola\\Asset\\Resolver\\ResolverInterface."
            );
        }
        $this->aggregateResolver = $resolver ;

        return $this ;
    }


    /**
     * Set the MimeResolver.
     *
     * @param MimeResolver $resolver
     */
    public function setMimeResolver(MimeResolver $resolver) {
        $this->mimeResolver = $resolver ;

        return $this ;
    }

    /**
     * Get the MimeResolver
     *
     * @return MimeResolver
     */
    public function getMimeResolver() {
        return $this->mimeResolver ;
    }

    /**
     * {@inheritDoc}
     */
    public function collect()
    {
        return array_keys($this->collections);
    }
}
