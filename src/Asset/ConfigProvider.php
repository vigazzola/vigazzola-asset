<?php
/**
 */

namespace Vigazzola\Asset ;

class ConfigProvider
{
    /**
     *
     * @return array
     */
    public function __invoke()
    { 
         return include __DIR__ . '/../../config/module.config.php';
        
    }
}
