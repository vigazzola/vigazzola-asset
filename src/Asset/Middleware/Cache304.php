<?php
/**
 * @author vigazzola@gmail.com
 */

namespace Vigazzola\Asset\Middleware;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Vigazzola\Asset\Cache\CacheInterface ;
use Vigazzola\Asset\Response\Response ;

class Cache304 implements MiddlewareInterface
{
    private $container ;
    private $cacher ;

    public function __construct( $container = null)
    {
        $this->container = $container ;
    }

    public function setCacher(CacheInterface $cacher) {
        $this->cacher = $cacher ;

        return $this ;
    }

    public function getCacher() {
        return $this->cacher ;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        $path = $request->getUri()->getPath() ?: '/';
        if(!($this->cacher && $this->cacher->contains($path))) return $handler->handle($request);

        $Response = new Response('', 304) ;

        return $Response;
    }

}
