<?php
/**
 * @author vigazzola@gmail.com
 */

namespace Vigazzola\Asset\Middleware;

use Psr\Container\ContainerInterface;

class DirectoriesFactory
{
    /**
     */
    public function __invoke(ContainerInterface $container, $response)
    {
        $Directories = new Directories($container) ;

        $Service = $container->get(\Vigazzola\Asset\Service\DirectoriesService::class) ;
        $Directories->setService($Service) ;

        return $Directories ;
    }
}
