<?php
/**
 * @author vigazzola@gmail.com
 */

namespace Vigazzola\Asset\Middleware;

use Psr\Container\ContainerInterface;

class MapFactory
{
    /**
     */
    public function __invoke(ContainerInterface $container, $response) 
    {
        $Map = new Map($container) ;

        $service = $container->get(\Vigazzola\Asset\Service\MapService::class) ;
        $Map->setService($service) ;
        
        return $Map ;
    }
}
