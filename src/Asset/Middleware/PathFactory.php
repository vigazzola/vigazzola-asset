<?php
/**
 * @author vigazzola@gmail.com
 */

namespace Vigazzola\Asset\Middleware;

use Psr\Container\ContainerInterface;

class PathFactory
{
    /**
     */
    public function __invoke(ContainerInterface $container, $response) 
    {
        $Path = new Path($container) ;

        $service = $container->get(\Vigazzola\Asset\Service\PathStackService::class) ;
        $Path->setService($service) ;
        
        return $Path ;
    }
}
