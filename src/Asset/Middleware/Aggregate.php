<?php
/**
 * @author vigazzola@gmail.com
 */

namespace Vigazzola\Asset\Middleware;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Vigazzola\Asset\Service\AssetManagerInterface ;

class Aggregate implements MiddlewareInterface
{
    private $container ;
    private $service ;

    public function __construct( $container = null)
    {
        $this->container = $container ;
    }

    public function setService(AssetManagerInterface $service) {
        $this->service = $service ;

        return $this ;
    }

    public function getService() {
        return $this->service ;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        if (!$this->service->resolvesToAsset($request)) return $handler->handle($request);
        return $this->service->setAssetOnResponse();
    }

}
