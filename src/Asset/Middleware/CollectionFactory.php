<?php
/**
 * @author vigazzola@gmail.com
 */

namespace Vigazzola\Asset\Middleware;

use Psr\Container\ContainerInterface;

class CollectionFactory
{
    /**
     */
    public function __invoke(ContainerInterface $container, $response)
    {
        $Collection = new Collection($container) ;

        $Service = $container->get(\Vigazzola\Asset\Service\CollectionService::class) ;
        $Collection->setService($Service) ;

        return $Collection ;
    }
}
