<?php
/**
 * @author vigazzola@gmail.com
 */

namespace Vigazzola\Asset\Middleware;

use Psr\Container\ContainerInterface;

class Cache304Factory
{
    /**
     */
    public function __invoke(ContainerInterface $container, $response)
    {
        $Middleware = new Cache304($container) ;

        $cacher = $container->get('Asset.Cache.Provider') ;
        $Middleware->setCacher($cacher) ;

        return $Middleware ;
    }
}
