<?php
/**
 * @author vigazzola@gmail.com
 */

namespace Vigazzola\Asset\Middleware;

use Psr\Container\ContainerInterface;

class AggregateFactory
{
    /**
     */
    public function __invoke(ContainerInterface $container, $response)
    {
        $Aggregate = new Aggregate($container) ;

        $service = $container->get(\Vigazzola\Asset\Service\AggregateService::class) ;
        $Aggregate->setService($service) ;

        return $Aggregate ;
    }
}
