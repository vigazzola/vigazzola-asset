<?php
/**
* @author vigazzola@gmail.com
*/
namespace Vigazzola\Asset\Service ;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Assetic\Asset\AssetInterface;
use Vigazzola\Asset\Response\Response ;
use Vigazzola\Asset\Cache\CacheInterface ;
use Vigazzola\Asset\Resolver\ResolverInterface ;


class CacheService implements AssetManagerInterface
{
    private $service ;
    private $cache ;
    private $lifeTime = 0 ;
    private $path ;

    public function setService(AssetManagerInterface $service) {
        $this->service = $service ;

        return $this ;
    }

    public function getService() {
        return $this->service ;
    }

    public function setCache(CacheInterface $cache) {
        $this->cache = $cache ;

        return $this ;
    }

    public function getCache() {
        return $this->cache ;
    }

    public function setLifetime($lifeTime) {
        $this->lifeTime = $lifeTime ;

        return $this ;
    }

    public function getLifetime() {
        return $this->lifeTime ;
    }

    /**
     * Check if the request resolves to an asset.
     *
     * @param    RequestInterface $request
     * @return   boolean
     */
    public function resolvesToAsset(ServerRequestInterface $request) {
        $this->path = $request->getUri()->getPath() ?: '/';

        if($this->cache && $this->cache->contains($this->path)) return true ;
        return $this->service->resolvesToAsset($request) ;
    }

    /**
     * Set the asset on the response, including headers and content.
     *
     * @param    ResponseInterface $response
     * @return   ResponseInterface
     * @throws   \Exception
     */
    public function setAssetOnResponse() {
        if(!($this->path && $this->cache && $this->cache->contains($this->path))) {
            $asset      = $this->service->getAsset() ;
            if (!$asset instanceof AssetInterface) {
                throw new \Exception(
                    'Unable to set asset on response. Request has not been resolved to an asset.'
                );
            }

            $Response   = $this->service->setAssetOnResponse() ;

            if($this->cache) {
                $mimeType       = $asset->mimetype;
                $assetContents  = $asset->dump();

                if (function_exists('mb_strlen')) {
                    $contentLength = mb_strlen($assetContents, '8bit');
                } else {
                    $contentLength = strlen($assetContents);
                }
                $data = [
                    'Content-Transfer-Encoding' => 'binary',
                    'expires'                   => gmdate("D, d M Y H:i:s T", time()+$this->lifeTime),
                    'Content-Type'              => $mimeType,
                    'Content-Length'            => $contentLength,
                    'AssetContents'             => $assetContents
                ] ;
                $this->cache->save($this->path, $data, $this->lifeTime);
            }

            return $Response ;
        }

        $data = $this->cache->fetch($this->path);

        $Response = new Response($data['AssetContents'], 200, [
            'expires'                   => $data['expires'],
            'Content-Transfer-Encoding' => $data['Content-Transfer-Encoding'],
            'Content-Type'              => $data['Content-Type'],
            'Content-Length'            => $data['Content-Length']
        ]) ;
        $this->assetSetOnResponse = true;

        return $Response ;
    }

    /**
     * Set the resolver to use in the asset manager
     *
     * @param ResolverInterface $resolver
     */
    public function setResolver(ResolverInterface $resolver) {
        throw new \Exception("setResolver not implemented");

    }

    public function getResolver() {
        throw new \Exception("getResolver not implemented");
    }

    public function getAsset() {
        return $this->service->getAsset() ;
    }

}
