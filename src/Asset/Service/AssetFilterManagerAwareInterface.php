<?php
/**
 *
 * @author vigazzola@gmail.com
 */
namespace Vigazzola\Asset\Service;

use Vigazzola\Asset\Filter\AssetFilterManagerInterface ;

interface AssetFilterManagerAwareInterface
{
    /**
     * Set the AssetFilterManager.
     *
     * @param AssetFilterManager $filterManager
     */
    public function setAssetFilterManager(AssetFilterManagerInterface $filterManager);

    /**
     * Get the AssetFilterManager
     *
     * @return AssetFilterManager
     */
    public function getAssetFilterManager();
}
