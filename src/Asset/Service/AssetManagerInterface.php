<?php
/**
 * @author vigazzola@gmail.com
 */
namespace Vigazzola\Asset\Service ;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Vigazzola\Asset\Resolver\ResolverInterface ;

interface AssetManagerInterface
{
    /**
     * Set the resolver to use in the asset manager
     *
     * @param ResolverInterface $resolver
     */
    public function setResolver(ResolverInterface $resolver) ;

    public function getResolver() ;

    /**
     * Check if the request resolves to an asset.
     *
     * @param    RequestInterface $request
     * @return   boolean
     */
    public function resolvesToAsset(ServerRequestInterface $request) ;

    /**
     * Set the asset on the response, including headers and content.
     *
     * @param    ResponseInterface $response
     * @return   ResponseInterface
     * @throws   \Exception
     */
    public function setAssetOnResponse() ;

    public function getAsset() ;
}
