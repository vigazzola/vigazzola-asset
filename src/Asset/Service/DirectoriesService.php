<?php
/**
 *
 */
namespace Vigazzola\Asset\Service ;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Assetic\Asset\AssetInterface;
use Vigazzola\Asset\Resolver\ResolverInterface ;
use Vigazzola\Asset\Response\Response ;
use Vigazzola\Asset\Filter\AssetFilterManagerInterface ;

class DirectoriesService implements AssetManagerInterface, AssetFilterManagerAwareInterface
{
    private $resolver       = null ;
    private $asset          = null ;
    private $filterManager  = null ;
    private $path           = null ;

    /**
     * Set the AssetFilterManager.
     *
     * @param AssetFilterManager $filterManager
     */
    public function setAssetFilterManager(AssetFilterManagerInterface $filterManager) {
        $this->filterManager = $filterManager ;

        return $this ;
    }

    /**
     * Get the AssetFilterManager
     *
     * @return AssetFilterManager
     */
    public function getAssetFilterManager() {
        return $this->filterManager ;
    }

    /**
     * Set the resolver to use in the asset manager
     *
     * @param ResolverInterface $resolver
     */
    public function setResolver(ResolverInterface $resolver) {
        $this->resolver = $resolver ;

        return $this ;
    }

    public function getResolver() {
        return $this->resolver ;
    }

    public function getAsset() {
        return $this->asset ;
    }

    /**
     * Check if the request resolves to an asset.
     *
     * @param    RequestInterface $request
     * @return   boolean
     */
    public function resolvesToAsset(ServerRequestInterface $request) {
        $this->path     = $request->getUri()->getPath() ?: '/';
        $this->asset    = $this->resolver->resolve($this->path, $this->getAssetFilterManager());

        return (bool)$this->asset;
    }

    /**
     * Set the asset on the response, including headers and content.
     *
     * @param    ResponseInterface $response
     * @return   ResponseInterface
     * @throws   \Exception
     */
    public function setAssetOnResponse() {
        if (!$this->asset instanceof AssetInterface) {
            throw new \Exception(
                'Unable to set asset on response. Request has not been resolved to an asset.'
            );
        }

        // @todo: Create Asset wrapper for mimetypes
        if (empty($this->asset->mimetype)) {
            throw new \Exception( 'Expected property "mimetype" on asset.');
        }

        $mimeType       = $this->asset->mimetype;
        $assetContents  = $this->asset->dump();


        // @codeCoverageIgnoreStart
        if (function_exists('mb_strlen')) {
            $contentLength = mb_strlen($assetContents, '8bit');
        } else {
            $contentLength = strlen($assetContents);
        }
        // @codeCoverageIgnoreEnd

        if (!empty($this->config['clear_output_buffer']) && $this->config['clear_output_buffer']) {
            // Only clean the output buffer if it's turned on and something
            // has been buffered.
            if (ob_get_length() > 0) {
                ob_clean();
            }
        }

        $Response = new Response($assetContents, 200, [
            'Content-Transfer-Encoding' => 'binary',
            'Content-Type'              => $mimeType,
            'Content-Length'            => $contentLength
        ]) ;
        $this->assetSetOnResponse = true;

        return $Response ;
    }

}
