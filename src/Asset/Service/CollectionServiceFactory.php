<?php
/**
 * @author vigazzola@gmail.com
 */
namespace Vigazzola\Asset\Service ;

use Psr\Container\ContainerInterface;

class CollectionServiceFactory
{
    /**
     */
    public function __invoke(ContainerInterface $container)
    {
        $CacheProvider  = $container->get('Asset.Cache.Provider') ;
        $lifeTime       = intval($container->get('Asset.Cache.LifeTime')) ;
        $Service        = new CollectionService();
        $Resolve        = $container->get(\Vigazzola\Asset\Resolver\CollectionResolver::class) ;
        $FilterManager  = $container->get(\Vigazzola\Asset\Filter\FilterPluginManager::class) ;
        $CacheService   = new CacheService() ;

        $Resolve->setAggregateResolver($container->get(\Vigazzola\Asset\Resolver\AggregateResolver::class)) ;
        $Service
            ->setResolver($Resolve)
            ->setAssetFilterManager($FilterManager) ;

        if(!$CacheProvider) return $Service ;

        $CacheService
            ->setService($Service)
            ->setLifetime($lifeTime)
            ->setCache($CacheProvider)
        ;
        return $CacheService ;
    }
}
