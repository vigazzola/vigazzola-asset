<?php
/**
 * @author vigazzola@gmail.com
 */
namespace Vigazzola\Asset\Service ;

use Psr\Container\ContainerInterface;

class AggregateServiceFactory
{
    /**
     */
    public function __invoke(ContainerInterface $container)
    {
        $CacheProvider  = $container->get('Asset.Cache.Provider') ;
        $lifeTime       = intval($container->get('Asset.Cache.LifeTime')) ;
        $Service        = new AggregateService();
        $Resolve        = $container->get(\Vigazzola\Asset\Resolver\AggregateResolver::class) ;
        $FilterManager  = $container->get(\Vigazzola\Asset\Filter\FilterPluginManager::class) ;
        $CacheService   = new CacheService() ;

        $Service
            ->setResolver($Resolve)
            ->setAssetFilterManager($FilterManager) ;

        if(!$CacheProvider) return $Service ;

        $CacheService
            ->setService($Service)
            ->setLifetime($lifeTime)
            ->setCache($CacheProvider)
        ;
        return $CacheService ;
    }
}
