<?php
/**
 * @author vigazzola@gmail.com
 */
namespace Vigazzola\Asset\Response ;

use Psr\Http\Message\StreamInterface;
use Zend\Diactoros\Response as DiactorosResponse ;
use Zend\Diactoros\Stream;

class Response extends DiactorosResponse
{
    use \Zend\Diactoros\Response\InjectContentTypeTrait;

    public function __construct($body, $status = 200, array $headers = [])
    {
        parent::__construct(
            $this->createBody($body),
            $status,
            $headers
        );
    }

    /**
     * Create the message body.
     *
     * @param string|StreamInterface $html
     * @return StreamInterface
     * @throws InvalidArgumentException if $html is neither a string or stream.
     */
    private function createBody($html)
    {
        if ($html instanceof StreamInterface) {
            return $html;
        }
        if (! is_string($html)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid content (%s) provided to %s',
                (is_object($html) ? get_class($html) : gettype($html)),
                __CLASS__
            ));
        }
        $body = new Stream('php://temp', 'wb+');
        $body->write($html);
        $body->rewind();
        return $body;
    }
}
