<?php
/**
 * Incapsula i dati in una funzione Javascript richiamando il templater:
 *
 * JavaScript Templates
 * https://github.com/blueimp/JavaScript-Templates
 *
 * La funzione sarà richiamabile con:
 * JST["{NAME}"]
 *
 * dove "NAME" è il path del file incapsulato senza estensione.
 * Se al filtro viene passata l'opzione 'path', NAME sarà il path del file,
 * meno il path passato. Altrimenti sarà solo il nome del file.
 *
 * @author      vigazzola@gmail.com
 * @link
 * @copyright
 * @license
 *
 **/
namespace Vigazzola\Asset\Filter ;

use Assetic\Asset\AssetInterface;
use Assetic\Filter\FilterInterface;

class TmplDataFilter implements FilterInterface
{
    protected $options;

    public function __construct(array $options = array()) {
        $this->options = $options;
    }

    public function filterLoad(AssetInterface $asset)
    {
    }

    public function filterDump(AssetInterface $asset)
    {
        $path   = $asset->getSourceRoot() . '/' . $asset->getSourcePath() ;
        $ext    = '' ;

        switch($asset->mimetype) {
            case 'text/php':
            case 'text/x-php':
            case 'application/php':
            case 'application/x-php':
            case 'application/x-httpd-php':
            case 'application/x-httpd-php': {
                $file   = $asset->getSourceRoot() . DIRECTORY_SEPARATOR . $asset->getSourcePath() ;
                $ext    = pathinfo($file, PATHINFO_EXTENSION);
                   try {
                       ob_start();
                       $ret = include $file ;
                       $template = ob_get_clean();
                   } catch (Exception $ex) {
                       ob_end_clean();
                       $template = "" ;
                   }

                   $template = preg_replace('~>\\s+<~m', '><', $template);
            } break ;
            case 'application/javascript':
            case 'text/javascript': {
                $file   = $asset->getSourceRoot() . DIRECTORY_SEPARATOR . $asset->getSourcePath() ;
                $ext    = pathinfo($file, PATHINFO_EXTENSION);

                $template  = $asset->getContent();
                $template = preg_replace('~>\\s+<~m', '><', $template);
            } break ;
            case 'text/plain': {
                $file   = $asset->getSourceRoot() . DIRECTORY_SEPARATOR . $asset->getSourcePath() ;
                $ext    = pathinfo($file, PATHINFO_EXTENSION);

                $template  = $asset->getContent();
                $template = preg_replace('~>\\s+<~m', '><', $template);
            } break ;
            case 'text/html': {
                $file   = $asset->getSourceRoot() . DIRECTORY_SEPARATOR . $asset->getSourcePath() ;
                $ext    = pathinfo($file, PATHINFO_EXTENSION);

                $template  = $asset->getContent();
                $template = preg_replace('~>\\s+<~m', '><', $template);
            } break ;
            default: return ;
        }

        // Base path
        $basePath   = realpath((isset($this->options['path']) && !empty($this->options['path'])) ? $this->options['path']: $asset->getSourceRoot()) ;
        $path       = realpath($path) ;
        $baseName   = '' ;
        if(strstr($path, $basePath) !== false) {
            $baseName = substr($path, strlen($basePath)+1) ;
        }
        $template = str_replace(
            array("\n", "\r", "'"),
            array("\\n", "\\r", "\\'"),
            $template
        ) ;
        if(!strlen($template)) return ;

        $name = str_replace(".$ext", "", $baseName) ;

        $template = str_replace(
            array("{NAME}", "{SOURCE}"),
            array($name, $template),
            $this->getMatrix()
            ) ;

        $asset->setContent($template );
    }

    private function getMatrix()
	{
$matrix = <<<"FOOBAR"

		(function() {
		  this.JST || (this.JST = {});
		  this.JST["{NAME}"] = function(__obj) {
			if (!__obj) __obj = {};

			return tmpl('{SOURCE}', __obj) ;
		  };
		}).call(this);

FOOBAR;
        return $matrix ;
    }

}
