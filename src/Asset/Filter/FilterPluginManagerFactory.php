<?php
/**
 * @author vigazzola@gmail.com
 */
 namespace Vigazzola\Asset\Filter ;

 use Interop\Container\ContainerInterface;
 use Zend\ServiceManager\Config;
 use Zend\ServiceManager\FactoryInterface;
 use Zend\ServiceManager\ServiceLocatorInterface;

 class FilterPluginManagerFactory implements FactoryInterface
 {
     /**
      * zend-servicemanager v2 support for invocation options.
      *
      * @param array
      */
     protected $creationOptions;

     /**
      * {@inheritDoc}
      *
      * @return FilterPluginManager
      */
     public function __invoke(ContainerInterface $container, $name, array $options = null)
     {
         // If we do not have a config service, nothing more to do
         if (! $container->has('config')) {
             return  new FilterPluginManager($container, $options ?: []);
         }

         // If this is in a zend-mvc application, the ServiceListener will inject
         // merged configuration during bootstrap.
         if ($container->has('ServiceListener')) {
             return  new FilterPluginManager($container, $options ?: []);
         }

         $config = $container->get('config');

         // If we do not have filters configuration, nothing more to do
         if (! isset($config['asset']) || ! is_array($config['asset'])) {
             return  new FilterPluginManager($container, $options ?: []);
         }

         $dependencies = [] ;
         if(isset($config['asset']['filters']['dependencies']['aliases']) && is_array($config['asset']['filters']['dependencies']['aliases'])) {
             $dependencies['aliases'] = $config['asset']['filters']['dependencies']['aliases'] ;
             //$pluginManager->mergeAliases($config['asset']['filters']['dependencies']['aliases']) ;
         }

         if(isset($config['asset']['filters']['dependencies']['factories']) && is_array($config['asset']['filters']['dependencies']['factories'])) {
             $dependencies['factories'] = $config['asset']['filters']['dependencies']['factories'] ;
         }
         $pluginManager = new FilterPluginManager($container, $options ? array_merge($options, $dependencies): $dependencies);

         // Wire service configuration for validators
         (new Config($config['asset']))->configureServiceManager($pluginManager);

         $pluginManager->setConfigure($config['asset']['filters']) ;

         $pluginManager->setMimeResolver($container->get(\Vigazzola\Asset\Resolver\MimeResolver::class)) ;

         return $pluginManager;
     }

     /**
      * {@inheritDoc}
      *
      * @return FilterPluginManager
      */
     public function createService(ServiceLocatorInterface $container, $name = null, $requestedName = null)
     {
         return $this($container, $requestedName ?: FilterPluginManager::class, $this->creationOptions);
     }

     /**
      * zend-servicemanager v2 support for invocation options.
      *
      * @param array $options
      * @return void
      */
     public function setCreationOptions(array $options)
     {
         $this->creationOptions = $options;
     }
 }
