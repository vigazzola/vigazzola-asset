<?php
/**
 * @author vigazzola@gmail.com
 */
namespace Vigazzola\Asset\Filter ;

use Assetic\Asset\AssetInterface ;
use Vigazzola\Asset\Resolver\MimeResolver ;

interface AssetFilterManagerInterface
{
    /**
     * See if there are filters for the asset, and if so, set them.
     *
     * @param   string          $path
     * @param   AssetInterface  $asset
     *
     * @throws Exception\RuntimeException on invalid filters
     */
    public function setFilters($path, AssetInterface $asset) ;

    /**
     * {@inheritDoc}
     */
    public function getMimeResolver() ;

    /**
     * {@inheritDoc}
     */
    public function setMimeResolver(MimeResolver $resolver) ;

}
