<?php
/**
 */
namespace Vigazzola\Asset\Filter ;

use Zend\ServiceManager\AbstractPluginManager;
use Zend\ServiceManager\Exception\InvalidServiceException;
use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\Stdlib\PriorityQueue;
use Assetic\Asset\AssetInterface ;
use Vigazzola\Asset\Resolver\MimeResolver ;


/**
 * Plugin manager implementation for the filter chain.
 *
 * Enforces that filters retrieved are either callbacks or instances of
 * FilterInterface. Additionally, it registers a number of default filters
 * available, as well as aliases for them.
 */
class FilterPluginManager extends AbstractPluginManager implements AssetFilterManagerInterface
{
    protected $aliases = [

        // For the future
        'AddFileNameFilter'         => AddFileNameFilter::class,
        'addFileNameFilter'         => AddFileNameFilter::class,
        'addfilenamefilter'         => AddFileNameFilter::class,

        'TmplDataFilter'            => TmplDataFilter::class,
        'tmplDataFilter'            => TmplDataFilter::class,
        'tmpldatafilter'            => TmplDataFilter::class,

    ];

    /**
     * Default set of plugins factories
     *
     * @var array
     */
    protected $factories = [
        AddFileNameFilter::class        => InvokableFactory::class,
        TmplDataFilter::class           => InvokableFactory::class,
    ];

    protected $instanceOf = \Assetic\Filter\FilterInterface::class;

    /**
     * Whether or not to share by default; default to false (v2)
     *
     * @var bool
     */
    protected $shareByDefault = false;

    /**
     * @var MimeResolver
     */
    protected $mimeResolver;

    protected $assetConfigure ;

    /**
     * See if there are filters for the asset, and if so, set them.
     *
     * @param   string          $path
     * @param   AssetInterface  $asset
     *
     * @throws \Exception on invalid filters
     */
    public function setFilters($path, AssetInterface $asset)
    {
        $config = $this->getConfigure() ;

        if (!empty($config['filters'][$path])) {
            $filters = $config['filters'][$path];
        } elseif (!empty($config[$asset->mimetype])) {
            $filters = $config[$asset->mimetype];
        } else {
            $extension = $this->getMimeResolver()->getExtension($asset->mimetype);
            if (!empty($config[$extension])) {
                $filters = $config[$extension];
            } else {
                return;
            }
        }

        $queue      = new PriorityQueue();
        $priority   = 1 ;
        foreach ($filters as $filter) {
            if (is_string($filter)) {
                $filter = $this->get($filter);

                $queue->insert($filter, $priority);

                continue ;
            }

            if (!is_array($filter) && !$filter instanceof ArrayAccess) {
                throw new \Exception(sprintf(
                    'Provided path must be an array or an instance of ArrayAccess, %s given',
                    is_object($filter) ? get_class($filter) : gettype($filter)
                ));
            }

            if (isset($filter['priority']) && isset($filter['filter'])) {
                $objFilter = (isset($filter['options']) && is_array($filter['options'])) ? $this->build($filter['filter'], $filter['options']) : $this->get($filter['filter']);
                $queue->insert($objFilter, $filter['priority']);

                continue ;
            }

            throw new \Exception( 'Provided array must contain both keys "priority" and "path"');
        }

        foreach ($queue as $filter) {
            $asset->ensureFilter($filter);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function validate($plugin)
    {
        if ($plugin instanceof $this->instanceOf) {
            // we're okay
            return;
        }

        if (is_callable($plugin)) {
            // also okay
            return;
        }

        throw new InvalidServiceException(sprintf(
            'Plugin of type %s is invalid; must implement %s\FilterInterface or be callable',
            (is_object($plugin) ? get_class($plugin) : gettype($plugin)),
            __NAMESPACE__
        ));
    }

    /**
     * {@inheritDoc}
     */
    public function getMimeResolver()
    {
        return $this->mimeResolver;
    }

    /**
     * {@inheritDoc}
     */
    public function setMimeResolver(MimeResolver $resolver)
    {
        $this->mimeResolver = $resolver;

        return $this ;
    }

    /**
     * {@inheritDoc}
     */
    public function getConfigure()
    {
        return $this->assetConfigure ;
    }

    /**
     * {@inheritDoc}
     */
    public function setConfigure($assetConfigure)
    {
        $this->assetConfigure = $assetConfigure;

        return $this ;
    }

    public function mergeAliases(array $aliases) {
        $this->aliases = array_merge($this->aliases, $aliases) ;

        return $this ;
    }

    public function mergeFactories(array $factories) {
        $this->factories = array_merge($this->factories, $factories) ;

        return $this ;
    }

}
