<?php

/**
 *
 *
 * @author      vigazzola@gmail.com
 * @link
 * @copyright
 * @license
 *
 **/
namespace Vigazzola\Asset\Filter ;

use Assetic\Asset\AssetInterface;
use Assetic\Filter\FilterInterface;


/**
 * Add File Name by JS and CSS
 *
 */
class AddFileNameFilter implements FilterInterface
{
    protected $options;

    public function __construct(array $options = array()) {
        $this->options = $options;
    }

    public function filterLoad(AssetInterface $asset)
    {
    }

    public function filterDump(AssetInterface $asset)
    {
        $path       = $asset->getSourceRoot() . '/' . $asset->getSourcePath() ;
        $content    = "////////////////////" ;

        switch($asset->mimetype) {
            case 'application/javascript':  $content = "\n// FILENAME: " . $path . "\n" ; break ;
            case 'text/css':                $content = "\n/* FILENAME: " . $path . "*/\n" ; break ;
            case 'application/x-httpd-php': $content = "\n// FILENAME: " . $path . "\n" ; break ;
        }

        $asset->setContent($content . $asset->getContent() );
    }
}
