<?php
/**
 * @author vigazzola@gmail.com
 */
use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\MvcEvent;

namespace Vigazzola\Asset ;

class Module
{
    /**
     * Register a specification for the FormElementManager with the ServiceListener.
     *
     * @param \Zend\ModuleManager\ModuleManager $moduleManager
     * @return void
     */
    public function init(ModuleManager $manager)
    {
        // Get event manager.
        $eventManager           = $manager->getEventManager();
        $sharedEventManager     = $eventManager->getSharedManager();
        
        $priority     = -9999999;
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, [$this, 'onDispatch'], $priority);
        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, [$this, 'onDispatch'], $priority);        
    }
    
    // Event listener method.
    public function onDispatch(MvcEvent $event)
    {
        // TODO      
    }
        

    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }

}
