<?php
/*
 */
 namespace Vigazzola\Asset\Cache ;

 use MongoDB\BSON\Binary;
 use MongoDB\BSON\UTCDateTime;
 use MongoDB\Collection;
 use MongoDB\Database;
 use MongoDB\Driver\Exception\Exception;
 use MongoDB\Model\BSONDocument;


/**
 * MongoDB cache provider.
 *
 * @since  1.1
 * @author Jeremy Mikola <jmikola@gmail.com>
 */
class MongoDBCache implements CacheInterface
{
    /**
     * The data field will store the serialized PHP value.
     */
    const DATA_FIELD = 'd';

    /**
     * Cache created date time
     */
    const CREATED_FIELD = 'c';

    /**
     * @see http://docs.mongodb.org/manual/tutorial/expire-data/
     */
    const EXPIRATION_FIELD = 'e';

    /**
     * @var Database
     */
    private $database;

    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var bool
     */
    private $expirationIndexCreated = false;

    /**
     * Constructor.
     *
     * This provider will default to the write concern and read preference
     * options set on the Database instance (or inherited from MongoDB or
     * Client). Using an unacknowledged write concern (< 1) may make the return
     * values of delete() and save() unreliable. Reading from secondaries may
     * make contain() and fetch() unreliable.
     *
     * @see http://www.php.net/manual/en/mongo.readpreferences.php
     * @see http://www.php.net/manual/en/mongo.writeconcerns.php
     * @param Collection $collection
     */
    public function __construct(Collection $collection)
    {
        // Ensure there is no typemap set - we want to use our own
        $this->collection = $collection->withOptions(['typeMap' => null]);
        $this->database = new Database($collection->getManager(), $collection->getDatabaseName());
    }

    /**
     * {@inheritdoc}
     */
    public function fetch($id)
    {
        $document = $this->collection->findOne(['_id' => $id], [self::DATA_FIELD, self::EXPIRATION_FIELD]);

        if ($document === null) {
            return false;
        }

        if ($this->isExpired($document)) {
            $this->createExpirationIndex();
            $this->doDelete($id);
            return false;
        }

        return unserialize($document[self::DATA_FIELD]->getData());
    }

    /**
     * {@inheritdoc}
     */
    public function contains($id)
    {
        $document = $this->collection->findOne(['_id' => $id], [self::EXPIRATION_FIELD]);
        if ($document === null) {
            return false;
        }

        if ($this->isExpired($document)) {
            $this->createExpirationIndex();
            $this->delete($id);
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function save($id, $data, $lifeTime = 0)
    {
        try {
            $this->collection->updateOne(
                ['_id' => $id],
                ['$set' => [
                    self::EXPIRATION_FIELD  => ($lifeTime > 0 ? new UTCDateTime((time() + $lifeTime) * 1000): null),
                    self::DATA_FIELD        => new Binary(serialize($data), Binary::TYPE_GENERIC),
                    self::CREATED_FIELD     => new UTCDateTime((time()) * 1000)
                ]],
                ['upsert' => true]
            );
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function delete($id)
    {
        try {
            // Use remove() in lieu of drop() to maintain any collection indexes
            $this->collection->deleteMany([]);
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function flush()
    {
        // Use remove() in lieu of drop() to maintain any collection indexes
        $result = $this->collection->remove();

        return isset($result['ok']) ? $result['ok'] == 1 : true;
    }

    public function getCreatedTime($id) {
        $document = $this->collection->findOne(['_id' => $id], [self::EXPIRATION_FIELD, self::CREATED_FIELD]);
        if ($document === null) {
            return false;
        }

        if ($this->isExpired($document)) {
            $this->createExpirationIndex();
            $this->delete($id);
            return false;
        }

        $data = isset($document->{self::CREATED_FIELD})?$document->{self::CREATED_FIELD}:null ;
        if(!$data instanceof UTCDateTime) return false ;

        return $data->toDateTime()->getTimestamp() ;
    }

    /**
     * Check if the document is expired.
     *
     * @param array $document
     *
     * @return bool
     */
    private function isExpired(\MongoDB\Model\BSONDocument $document)
    {
        return isset($document->{self::EXPIRATION_FIELD}) &&
            $document->{self::EXPIRATION_FIELD} instanceof UTCDateTime &&
            $document->{self::EXPIRATION_FIELD}->toDateTime() < new \DateTime();
    }

    private function createExpirationIndex(): void
    {
        if ($this->expirationIndexCreated) {
            return;
        }

        $this->collection->createIndex([self::EXPIRATION_FIELD => 1], ['background' => true, 'expireAfterSeconds' => 0]);
    }

}
