<?php
use Zend\ServiceManager\Factory\InvokableFactory;
use Vigazzola\Asset\Cache\MongoDBCache ;

return [
    'asset' => [
        'resolver' => [
            'map'   => [

            ],
            'paths' => [
            ]
        ],
        'filters' => [
            'dependencies' => [
                'aliases'    => [
                        'JSMinFilter'       => \Assetic\Filter\JSMinFilter::class,
                        'jsMinFilter'       => \Assetic\Filter\JSMinFilter::class,
                        'jsminfilter'       => \Assetic\Filter\JSMinFilter::class,
                ],
                'factories' => [
                    \Assetic\Filter\JSMinFilter::class                  => InvokableFactory::class
                ]
            ],
        ],
        'cache' => [
            'MongoDB'=> [
                'params'    => [
                    'uri'            => 'mongodb://localhost:27017',
                    'uriOptions'     => [],
                    'driverOptions'  => [],
                    'db'             => 'xxxxxxxxx',
                ],
                'collection'  => 'cacheAsset'
            ],
            'LifeTime'  => 60       // Sec
        ]
    ],

    'dependencies' => [
        'factories' => [
            Vigazzola\Asset\Middleware\Map::class           => Vigazzola\Asset\Middleware\MapFactory::class,
            Vigazzola\Asset\Middleware\Path::class          => Vigazzola\Asset\Middleware\PathFactory::class,
            Vigazzola\Asset\Middleware\Aggregate::class     => Vigazzola\Asset\Middleware\AggregateFactory::class,
            Vigazzola\Asset\Middleware\Collection::class    => Vigazzola\Asset\Middleware\CollectionFactory::class,
            Vigazzola\Asset\Middleware\Directories::class   => Vigazzola\Asset\Middleware\DirectoriesFactory::class,
            Vigazzola\Asset\Middleware\Cache304::class      => Vigazzola\Asset\Middleware\Cache304Factory::class,

            Vigazzola\Asset\Service\MapService::class           => Vigazzola\Asset\Service\MapServiceFactory::class,
            Vigazzola\Asset\Service\PathStackService::class     => Vigazzola\Asset\Service\PathStackServiceFactory::class,
            Vigazzola\Asset\Service\AggregateService::class     => Vigazzola\Asset\Service\AggregateServiceFactory::class,
            Vigazzola\Asset\Service\CollectionService::class    => Vigazzola\Asset\Service\CollectionServiceFactory::class,
            Vigazzola\Asset\Service\DirectoriesService::class   => Vigazzola\Asset\Service\DirectoriesServiceFactory::class,

            Vigazzola\Asset\Resolver\MimeResolver::class        => Zend\ServiceManager\Factory\InvokableFactory::class,
            Vigazzola\Asset\Resolver\MapResolver::class         => Vigazzola\Asset\Resolver\MapResolverFactory::class,
            Vigazzola\Asset\Resolver\PathStackResolver::class   => Vigazzola\Asset\Resolver\PathStackResolverFactory::class,
            Vigazzola\Asset\Resolver\AggregateResolver::class   => Vigazzola\Asset\Resolver\AggregateResolverFactory::class,
            Vigazzola\Asset\Resolver\CollectionResolver::class  => Vigazzola\Asset\Resolver\CollectionResolverFactory::class,
            Vigazzola\Asset\Resolver\DirectoriesResolver::class => Vigazzola\Asset\Resolver\DirectoriesResolverFactory::class,

            Vigazzola\Asset\Resolvers::class        => function($container) {
                return [
                    Vigazzola\Asset\Resolver\MapResolver::class         => 4000,
                    Vigazzola\Asset\Resolver\CollectionResolver::class  => 2000,
                    Vigazzola\Asset\Resolver\DirectoriesResolver::class => 1000,
                    Vigazzola\Asset\Resolver\PathStackResolver::class   => 100
                ] ;
            },

            // Filters
            Vigazzola\Asset\Filter\FilterPluginManager::class => Vigazzola\Asset\Filter\FilterPluginManagerFactory::class,

            // Cache

            'Asset.Cache.Provider'         => function($sm, $name = null, $options = null) {
                $conf = $sm->get('config')['asset']['cache'] ;

                $client         = new \MongoDB\Client($conf['MongoDB']['params']['uri'], $conf['MongoDB']['params']['uriOptions'], $conf['MongoDB']['params']['driverOptions']) ;
                $nameCollection = $conf['MongoDB']['collection'] ;

                $Cacher         = new MongoDBCache($client->{$conf['MongoDB']['params']['db']}->{$nameCollection}) ;

                return $Cacher ;
            },
            'Asset.Cache.LifeTime' => function($sm, $name = null, $options = null) {
                $conf = $sm->get('config')['asset']['cache'] ;

                return $conf['LifeTime'] ;
            }
        ],

        'invokables' => [
        ],

     ]
];
